// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CODINGTEST_CodingTestGameModeBase_generated_h
#error "CodingTestGameModeBase.generated.h already included, missing '#pragma once' in CodingTestGameModeBase.h"
#endif
#define CODINGTEST_CodingTestGameModeBase_generated_h

#define CodingTest_Source_CodingTest_CodingTestGameModeBase_h_15_RPC_WRAPPERS
#define CodingTest_Source_CodingTest_CodingTestGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define CodingTest_Source_CodingTest_CodingTestGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACodingTestGameModeBase(); \
	friend struct Z_Construct_UClass_ACodingTestGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ACodingTestGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/CodingTest"), NO_API) \
	DECLARE_SERIALIZER(ACodingTestGameModeBase)


#define CodingTest_Source_CodingTest_CodingTestGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesACodingTestGameModeBase(); \
	friend struct Z_Construct_UClass_ACodingTestGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ACodingTestGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/CodingTest"), NO_API) \
	DECLARE_SERIALIZER(ACodingTestGameModeBase)


#define CodingTest_Source_CodingTest_CodingTestGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACodingTestGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACodingTestGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACodingTestGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACodingTestGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACodingTestGameModeBase(ACodingTestGameModeBase&&); \
	NO_API ACodingTestGameModeBase(const ACodingTestGameModeBase&); \
public:


#define CodingTest_Source_CodingTest_CodingTestGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACodingTestGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACodingTestGameModeBase(ACodingTestGameModeBase&&); \
	NO_API ACodingTestGameModeBase(const ACodingTestGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACodingTestGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACodingTestGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACodingTestGameModeBase)


#define CodingTest_Source_CodingTest_CodingTestGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define CodingTest_Source_CodingTest_CodingTestGameModeBase_h_12_PROLOG
#define CodingTest_Source_CodingTest_CodingTestGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodingTest_Source_CodingTest_CodingTestGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	CodingTest_Source_CodingTest_CodingTestGameModeBase_h_15_RPC_WRAPPERS \
	CodingTest_Source_CodingTest_CodingTestGameModeBase_h_15_INCLASS \
	CodingTest_Source_CodingTest_CodingTestGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CodingTest_Source_CodingTest_CodingTestGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CodingTest_Source_CodingTest_CodingTestGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	CodingTest_Source_CodingTest_CodingTestGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	CodingTest_Source_CodingTest_CodingTestGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	CodingTest_Source_CodingTest_CodingTestGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CODINGTEST_API UClass* StaticClass<class ACodingTestGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CodingTest_Source_CodingTest_CodingTestGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
